# MyDiro

Angular versão 13.1.2

## Instalação


### NodeJs

Instalar [NodeJs](https://github.com/nodesource/distributions/blob/master/README.md).

### myDiro

Colocar a pasta de deploy do myDiro na raiz do explore do sistema operacional.

Abrir terminal dentro da pasta myDiro, executar:

```bash
npm install
```

em seguida, executar:

```bash
node server.js
```

Aplicação vai rodar na url: [http://localhost:8082/](http://localhost:8082/)

### Back-End(Json Server)
Abrir terminal dentro da pasta myDiro/backend, executar:

```bash
npm install
```

em seguida, executar:

```bash
npm start
```
